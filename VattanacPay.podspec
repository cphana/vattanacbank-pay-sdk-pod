#
# Be sure to run `pod lib lint VattanacPay.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'VattanacPay'
  s.version          = '0.1'
  s.summary          = 'A short description of VattanacPay.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Phana Chhean' => 'cphana.vb@gmail.com' }
  #s.source           = { :path => './VattanacPaySDK.xcframework', :tag => s.version.to_s }

  s.source           = { :http => 'https://bitbucket.org/cphana/vattanacbank-pay-sdk-pod.git'}
  s.ios.deployment_target = '10.0'
  s.swift_version = '5.0'
  s.ios.vendored_frameworks = 'VattanacPaySDK.xcframework'
  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'


end
